# shoco

[![GoDoc](https://godoc.org/gitlab.com/storskegg/shoco?status.svg)](https://godoc.org/gitlab.com/storskegg/shoco)
[![Build Status](https://travis-ci.org/storskegg/shoco.svg?branch=master)](https://travis-ci.org/storskegg/shoco)

This repo is a fork from https://github.com/tmthrgd/shoco. It's been moved from my forked repo [here](https://github.com/storskegg/shoco) to resolve a type error stemming from the forked repo.

**shoco** is a Golang package, based on [the **shoco** C library](https://gitlab.com/Ed-von-Schleck/shoco), MIT licensed, to compress and decompress short strings. It is very fast and easy to use. The default compression model is optimized for english words, but it is possible to generate your own compression model based on your specific input data.

Compression models can be found in the [models package](https://godoc.org/gitlab.com/storskegg/shoco/models).

The default compression model is optimized for english words, but you can [generate your own compression model](#generating-compression-models) based on *your specific* input data.

## Download

```
go get gitlab.com/storskegg/shoco
```

## Generating Compression Models

**NOTE:** The following is copied from the original C-lib's repo. I've ported the original python script to output a go file that matches @tmthrgd's defined model, but otherwise have not tampered with logic in any way. Any unused variables have been left in place.

Maybe your typical input isn’t english words. Maybe it’s german or french – or whole sentences. Or file system paths. Or URLs. While the standard compression model of **shoco** should work for all of these, it might be worthwile to train **shoco** for this specific type of input data.

Fortunately, that’s really easy: **shoco** includes a python script called `generate_compression_model.py` that takes one or more text files and outputs a go file ready for **shoco** to use. You must give the model a name, which will be used for naming the lib's exported variable. Here’s an example that trains **shoco** with a dictionary (btw., not the best kind of training data, because it’s dominated by uncommon words):

```bash
$ ./generate_compression_model.py -n words -o models/words_model.go /usr/share/dict/words
```

There are options on how to chunk and strip the input data – for example, if we want to train **shoco** with the words in a readme file, but without punctuation and whitespace, we could do

```bash
$ ./generate_compression_model.py --split=whitespace --strip=punctuation -n readme README.md
```

Since we haven’t specified an output file, the resulting table file is printed on stdout.

This is most likely all you’ll need to generate a good model, but if you are adventurous, you might want to play around with all the options of the script: Type `generate_compression_model.py --help` to get a friendly help message. We won’t dive into the details here, though – just one word of warning: Generating tables can be slow if your input data is large, and _especially_ so if you use the `--optimize-encoding` option. Using [pypy](http://pypy.org/) can significantly speed up the process.

## Benchmark

```
BenchmarkCompress/#0-0-4         	126096571	      8.93 ns/op
BenchmarkCompress/#1-4-4         	16048700	      78.9 ns/op	  50.72 MB/s
BenchmarkCompress/#2-5-4         	 8190018	       155 ns/op	  32.18 MB/s
BenchmarkCompress/#3-240-4       	  366182	      3261 ns/op	  73.60 MB/s
BenchmarkCompress/#4-58-4        	 1598066	       718 ns/op	  80.83 MB/s
BenchmarkCompress/#5-20-4        	 3857050	       310 ns/op	  64.56 MB/s
BenchmarkCompress/#6-13-4        	 6331246	       192 ns/op	  67.86 MB/s
BenchmarkCompress/#7-111-4       	  615142	      1760 ns/op	  63.07 MB/s
BenchmarkCompress/#8-9-4         	 8188242	       149 ns/op	  60.31 MB/s
BenchmarkCompress/#9-13-4        	 6039622	       210 ns/op	  62.02 MB/s
BenchmarkCompress/#10-13-4       	 5606830	       196 ns/op	  66.50 MB/s
BenchmarkCompress/#11-10-4       	 6534748	       184 ns/op	  54.49 MB/s
BenchmarkCompress/#12-15-4       	 4984150	       212 ns/op	  70.84 MB/s
BenchmarkCompress/#13-35-4       	 2215468	       541 ns/op	  64.69 MB/s
BenchmarkCompress/#14-6-4        	10810364	       112 ns/op	  53.50 MB/s
BenchmarkCompress/#15-2-4        	18071437	      64.6 ns/op	  30.98 MB/s
BenchmarkCompress/#16-4-4        	13242044	      87.2 ns/op	  45.90 MB/s
BenchmarkCompress/#17-4-4        	12772329	      89.1 ns/op	  44.89 MB/s
BenchmarkCompress/#18-9-4        	 9566071	       123 ns/op	  72.97 MB/s
BenchmarkCompress/#19-2-4        	16556515	      61.2 ns/op	  32.66 MB/s
BenchmarkCompress/#20-4-4        	1864237               62.3 ns/op	  64.16 MB/s
BenchmarkCompress/#21-4-4        	15708982	      63.9 ns/op	  62.57 MB/s
BenchmarkDecompress/#0-0-4       	89143892	      13.3 ns/op
BenchmarkDecompress/#1-2-4       	13394772	      84.4 ns/op	  23.69 MB/s
BenchmarkDecompress/#2-3-4       	11130225	       116 ns/op	  25.86 MB/s
BenchmarkDecompress/#3-169-4     	  416620	      2437 ns/op	  69.35 MB/s
BenchmarkDecompress/#4-39-4      	 1773999	       706 ns/op	  55.25 MB/s
BenchmarkDecompress/#5-24-4      	 3889357	       309 ns/op	  77.68 MB/s
BenchmarkDecompress/#6-17-4      	 8547391	       146 ns/op	 116.14 MB/s
BenchmarkDecompress/#7-79-4      	 1000000	      1161 ns/op	  68.04 MB/s
BenchmarkDecompress/#8-18-4      	10759576	       105 ns/op	 171.52 MB/s
BenchmarkDecompress/#9-22-4      	 8610110	       138 ns/op	 159.93 MB/s
BenchmarkDecompress/#10-22-4     	 8774373	       131 ns/op	 168.11 MB/s
BenchmarkDecompress/#11-20-4     	10130929	       113 ns/op	 176.67 MB/s
BenchmarkDecompress/#12-25-4     	 8048341	       150 ns/op	 166.47 MB/s
BenchmarkDecompress/#13-46-4     	 3392990	       350 ns/op	 131.27 MB/s
BenchmarkDecompress/#14-12-4     	13885167	      82.1 ns/op	 146.08 MB/s
BenchmarkDecompress/#15-4-4      	22151634	      54.2 ns/op	  73.84 MB/s
BenchmarkDecompress/#16-8-4      	16822977	      68.9 ns/op	 116.17 MB/s
BenchmarkDecompress/#17-8-4      	14699456	      72.2 ns/op	 110.81 MB/s
BenchmarkDecompress/#18-6-4      	 9026619	       133 ns/op	  45.04 MB/s
BenchmarkDecompress/#19-3-4      	19061348	      54.2 ns/op	  55.39 MB/s
BenchmarkDecompress/#20-5-4      	21344918	      69.0 ns/op	  72.46 MB/s
BenchmarkDecompress/#21-5-4      	19424110	      52.8 ns/op	  94.75 MB/s
```

```
BenchmarkWords/Compress-4        	      24	  48785801 ns/op	  51.10 MB/s
BenchmarkWords/Decompress-4      	      42	  28691652 ns/op	  58.79 MB/s
```

## License

Unless otherwise noted, the shoco source files are distributed under the Modified BSD License
found in the LICENSE file.
